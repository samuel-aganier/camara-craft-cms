<?php
namespace Craft;

$subject = craft()->request->getPost('subject');
$subject = craft()->security->validateData($subject);

return array(
    'subject' => ($subject ?: null),
);

$Job = craft()->request->getPost('Job');
$Job = craft()->security->validateData($Job);

return array(
    'Job' => ($Job ?: null),
);


return [
    'toEmail'             => 'samuel@agencezel.com',
    'prependSubject'      => '',
    'prependSender'       => '',
    'allowAttachments'    => true,
    'successFlashMessage' => 'Message sent!'
];